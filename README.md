# Catalog Metadata

Le catalogue de la plateforme Open Mobility Indicators est constitué de l'ensemble des indicateurs (et leurs données produites par la pipeline de calcul). Ces indicateurs sont consultables en utilisant l'[API](https://api.openmobilityindicators.org).

Le catalogue présenté dans l'application Carto est personnalisable :
- affichage ou non de l'indicateur dans le sélecteur de couches (_pre_selected_)
- classement d'un indicateur dans une catégorie "en travaux" (_work_in_progress_)
- sélection de la liste des données d'indicateurs apparaissant pour un indicateur donné (_pre_selected_)

Cette personnalisation s'effectue dans le fichier [catalog-metadata.yml](catalog-metadata.yml). Ce fichier est utilisé par l'API (projet [omi-api](https://gitlab.com/open-mobility-indicators/omi-api)) pour compléter les informations relatives aux indicateurs et données d'indicateur avec la personnalisation souhaitée pour le catalogue.

## Mise à jour

Pour assurer que le contenu du fichier [catalog-metadata.yml](catalog-metadata.yml) soit toujours valide, il est interdit de le modifier directement. Toute mise à jour doit passer par la création d'une [merge request](https://gitlab.com/open-mobility-indicators/catalog-metadata/-/merge_requests) qui validera la structure du fichier ([pipeline CI](.gitlab-ci.yml) basée sur le projet [catalog-metadata-py](https://gitlab.com/open-mobility-indicators/catalog-metadata-py/))
